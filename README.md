### Install
PHP-7.4
MYSQL
COMPOSER

cd /var/www
git clone git@bitbucket.org:gorgona/grud-laravel.git folder
cd folder
composer install
define database, APP_KEY in .env
php artisan migrate --seed
php artisan serve
